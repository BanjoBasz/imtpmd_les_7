package com.example.pushdemoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "FROM MAINACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            Log.d("TEST1", "bundle " + bundle.get("leerdoel"));
            TextView tv = findViewById(R.id.textView);
            tv.setText(bundle.get("leerdoel").toString());
        }
    }
}
